package org.selenide.belkantrop;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.commands.Click;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.google.common.collect.ImmutableSet;
import io.qameta.allure.selenide.AllureSelenide;
import org.openqa.selenium.By;
import org.selenide.belkantrop.UsePages.Decor_Order_Page;
import org.selenide.belkantrop.UsePages.Product_Detail_Page;
import org.selenide.belkantrop.UsePages.Search_Page;
import org.selenide.belkantrop.UsePages.Start_Page;
import org.testng.Assert;
import org.testng.annotations.*;
//import sun.tools.tree.NewArrayExpression;

import java.util.Collections;

import static org.testng.Assert.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

@Test
public class NewTest {

    @BeforeTest
    public void setUp() {
        SelenideLogger.addListener("allure", new AllureSelenide());
    }

    @AfterTest
    public void tearDown() {
        SelenideLogger.removeListener("allure");
    }

    @BeforeMethod
    public void OpenSuit() {
        Configuration.startMaximized = true;
        open("https://mobilka.by/");
    }

    @AfterMethod
    public void CloseBrowser() {
        SelenideLogger.removeListener("allure");
        clearBrowserCookies();
        close();
    }

    //ТЕСТ C37413. Проверить наличие на новой странице фразы "Товары, соответствующие критериям поиска"
    public void C37413NEW() throws InterruptedException {
        Start_Page start_page = new Start_Page();
        start_page.checkLogoText("mObilka.by");
        start_page.setTextSearchField("Nokia");
        Assert.assertEquals("Nokia", start_page.getTextSearchField());
        start_page.clickOnSearchButton();
        Search_Page search_page = new Search_Page();
        search_page.checkTextOnSearchPage("Товары, соответствующие критериям поиска");
    }

    //ТЕСТ C37414. Проверить наличие на новой странице фразы "Основные характеристики".
    public void C37414NEW() throws InterruptedException {
        Start_Page start_page = new Start_Page();
        start_page.checkLogoText("mObilka.by");
        start_page.setTextSearchField("Nokia Lumia 800");
        Assert.assertEquals("Nokia Lumia 800", start_page.getTextSearchField());
        start_page.clickOnSearchButton();
        Search_Page search_page = new Search_Page();
        search_page.checkTextOnSearchPage("Товары, соответствующие критериям поиска");
        search_page.clickOnFirstElementRowProduct();
        Product_Detail_Page product_Detail_Page = new Product_Detail_Page();
        product_Detail_Page.checkTextOnPDPage("Основные характеристики");
    }

    //ТЕСТ C37415. Проверить увеличение значения поля "Кол-во" до 3.
    public void C37415NEW() throws InterruptedException {
        Start_Page start_page = new Start_Page();
        start_page.checkLogoText("mObilka.by");
        start_page.setTextSearchField("Nokia Lumia 800");
        Assert.assertEquals("Nokia Lumia 800", start_page.getTextSearchField());
        start_page.clickOnSearchButton();
        Search_Page search_page = new Search_Page();
        search_page.checkTextOnSearchPage("Товары, соответствующие критериям поиска");
        search_page.clickOnFirstElementRowProduct();
        Product_Detail_Page product_Detail_Page = new Product_Detail_Page();
        product_Detail_Page.checkTextOnPDPage("Основные характеристики");
        product_Detail_Page.setSomeProducts("3");
        Assert.assertEquals("3",product_Detail_Page.getCountOfSomeProducts());
    }

    //ТЕСТ C37416. Проверить увеличение цены в заданное количество раз. (Стоимость 290.00 р).
    public void C37416NEW() throws InterruptedException {
        Start_Page start_page = new Start_Page();
        start_page.checkLogoText("mObilka.by");
        start_page.setTextSearchField("Nokia Lumia 800");
        Assert.assertEquals("Nokia Lumia 800", start_page.getTextSearchField());
        start_page.clickOnSearchButton();
        Search_Page search_page = new Search_Page();
        search_page.checkTextOnSearchPage("Товары, соответствующие критериям поиска");
        search_page.clickOnFirstElementRowProduct();
        Product_Detail_Page product_Detail_Page = new Product_Detail_Page();
        product_Detail_Page.checkTextOnPDPage("Основные характеристики");
        product_Detail_Page.setSomeProducts("3");
        Thread.sleep(3000);
        Assert.assertEquals("3",product_Detail_Page.getCountOfSomeProducts());
        Assert.assertEquals("0.00 р.",product_Detail_Page.getAmountOfSomeProducts());
    }

    //ТЕСТ C37417. Проверить наличие кнопки "Оформить заказ".
    public void C37417NEW() throws InterruptedException {
        Start_Page start_page = new Start_Page();
        start_page.checkLogoText("mObilka.by");
        start_page.setTextSearchField("Nokia Lumia 800");
        Assert.assertEquals("Nokia Lumia 800", start_page.getTextSearchField());
        start_page.clickOnSearchButton();
        Search_Page search_page = new Search_Page();
        search_page.checkTextOnSearchPage("Товары, соответствующие критериям поиска");
        search_page.clickOnFirstElementRowProduct();
        Product_Detail_Page product_Detail_Page = new Product_Detail_Page();
        product_Detail_Page.checkTextOnPDPage("Основные характеристики");
        product_Detail_Page.clickOnFirstLittleImg();
        product_Detail_Page.checkTextOnButtonInBasket(" В корзину");
    }

    //ТЕСТ C37418. Проверить наличие текста "Оформление заказа".
    public void C37418NEW() throws InterruptedException {
        Start_Page start_page = new Start_Page();
        start_page.checkLogoText("mObilka.by");
        start_page.setTextSearchField("Nokia Lumia 800");
        Assert.assertEquals("Nokia Lumia 800", start_page.getTextSearchField());
        start_page.clickOnSearchButton();
        Search_Page search_page = new Search_Page();
        search_page.checkTextOnSearchPage("Товары, соответствующие критериям поиска");
        search_page.clickOnFirstElementRowProduct();
        Product_Detail_Page product_Detail_Page = new Product_Detail_Page();
        product_Detail_Page.checkTextOnPDPage("Основные характеристики");
        product_Detail_Page.clickOnFirstLittleImg();
        product_Detail_Page.checkTextOnButtonInBasket(" В корзину");
        product_Detail_Page.clickButtonInBasket();
        product_Detail_Page.checkTextOnButtonDecorOrder(" Оформить заказ");
        product_Detail_Page.clickOnButtonDecorOrder();
        Decor_Order_Page decor_Order_Page = new Decor_Order_Page();
        decor_Order_Page.checkTextOnDOPage("Оформление заказа");
    }

/*
    //ТЕСТ C37413. Проверить наличие на новой странице фразы "Товары, соответствующие критериям поиска"
    public void C37413() throws InterruptedException {
        Thread.sleep(3000);//ожидание подгрузки сайта
        //Проверка: ожидаем на странице текст "mObilka.by":
        $("#logo > span.text-primary").shouldHave(text("mObilka.by"));
        //Ввод в поле "Поиск" значения "Nokia":
        $(By.name("search")).setValue("Nokia");
        //Проверка: ожидаем текст "Nokia" в поле "Поиск":
        String expFieldSearch = "Nokia";//Ожидаемое значение
        String actFieldSearch = $(By.name("search")).getValue();//Фактическое значение
        System.out.println("Element is "+ actFieldSearch);//Фактическое выводим в логи
        Assert.assertEquals(expFieldSearch,actFieldSearch);//сравниваем ожидаемое и фактическое
        //Нажатие на кнопку "Поиск":
        $(By.cssSelector(".collapse > .input-group-btn .fa")).click();
        //Thread.sleep(3000);
        //Проверка: наличие на новой странице фразы: "Товары, соответствующие критериям поиска":
        $(By.xpath("//*[@id='content']/h2")).shouldHave(text("Товары, соответствующие критериям поиска"));
    }

//ТЕСТ C37414. Проверить наличие на новой странице фразы "Основные характеристики".
    public void C37414() throws InterruptedException {
        Thread.sleep(3000);//ожидание подгрузки сайта
        //Проверка: ожидаем на странице текст "mObilka.by"
        $("#logo > span.text-primary").shouldHave(text("mObilka.by"));
        //Ввод в поле "Поиск" значения "Nokia Lumia 800":
        $(By.name("search")).setValue("Nokia Lumia 800");
        //Проверка: ожидаем текст "Nokia Lumia 800" в поле "Поиск":
        String expFieldSearch = "Nokia Lumia 800";//Ожидаемое значение
        String actFieldSearch = $(By.name("search")).getValue();//Фактическое значение
        System.out.println("Element is "+ actFieldSearch);//Фактическое выводим в логи
        Assert.assertEquals(expFieldSearch,actFieldSearch);//сравниваем ожидаемое и фактическое
        //Нажатие на кнопку "Поиск":
        $(By.cssSelector(".collapse > .input-group-btn .fa")).click();
        //Проверка перехода на новую страницу: наличие фразы: "Товары, соответствующие критериям поиска":
        $(By.xpath("//*[@id='content']/h2")).shouldHave(text("Товары, соответствующие критериям поиска"));
        //Нажатие на картинку с мобильным телефоном:
        $(By.cssSelector("a > .img-responsive")).click();
        //Проверка перехода на новую страницу: наличие фразы: "Основные характеристики":
        $(By.xpath("//*[@id=\'tab-specification\']/div/table/tbody[1]/tr/th/strong")).shouldHave(text("Основные характеристики"));
    }

//ТЕСТ C37415. Проверить увеличение значения поля "Кол-во" до 3.
    public void C37415() throws InterruptedException {
        Thread.sleep(3000);//ожидание подгрузки сайта
        //Проверка: ожидаем на странице текст "mObilka.by"
        $("#logo > span.text-primary").shouldHave(text("mObilka.by"));
        //Ввод в поле "Поиск" значения "Nokia Lumia 800":
        $(By.name("search")).setValue("Nokia Lumia 800");
        //Проверка: ожидаем текст "Nokia Lumia 800" в поле "Поиск":
        String expFieldSearch = "Nokia Lumia 800";//Ожидаемое значение
        String actFieldSearch = $(By.name("search")).getValue();//Фактическое значение
        System.out.println("Element is "+ actFieldSearch);//Фактическое выводим в логи
        Assert.assertEquals(expFieldSearch,actFieldSearch);//сравниваем ожидаемое и фактическое
        //Нажатие на кнопку "Поиск":
        $(By.cssSelector(".collapse > .input-group-btn .fa")).click();
        //Проверка перехода на новую страницу: наличие фразы: "Товары, соответствующие критериям поиска":
        $(By.xpath("//*[@id='content']/h2")).shouldHave(text("Товары, соответствующие критериям поиска"));
        //Нажатие на картинку с мобильным телефоном:
        $(By.cssSelector("a > .img-responsive")).click();
        //Проверка перехода на новую страницу: наличие фразы: "Основные характеристики":
        $(By.xpath("//*[@id=\'tab-specification\']/div/table/tbody[1]/tr/th/strong")).shouldHave(text("Основные характеристики"));
        //Нажатие на стрелку вверх два раза в поле "Кол-во";
        $(By.id("input-quantity")).setValue("3");
        //Проверка значения в поле "Кол-во":
        String expFieldAmount = "3";//Ожидаемое значение
        String actFieldAmount = $(By.xpath("//*[@id=\'input-quantity\']")).getValue();//Фактическое значение
        System.out.println("Element is "+ actFieldAmount);//Фактическое выводим в логи
        Assert.assertEquals(expFieldAmount,actFieldAmount);//сравниваем ожидаемое и фактическое
    }

//ТЕСТ C37416. Проверить увеличение цены в заданное количество раз. (Стоимость 290.00 р).
    public void C37416() throws InterruptedException {
        Thread.sleep(3000);//ожидание подгрузки сайта
        //Проверка: ожидаем на странице текст "mObilka.by"
        $("#logo > span.text-primary").shouldHave(text("mObilka.by"));
        //Ввод в поле "Поиск" значения "Nokia Lumia 800":
        $(By.name("search")).setValue("Nokia Lumia 800");
        //Проверка: ожидаем текст "Nokia Lumia 800" в поле "Поиск":
        String expFieldSearch = "Nokia Lumia 800";//Ожидаемое значение
        String actFieldSearch = $(By.name("search")).getValue();//Фактическое значение
        System.out.println("Element is "+ actFieldSearch);//Фактическое выводим в логи
        Assert.assertEquals(expFieldSearch,actFieldSearch);//сравниваем ожидаемое и фактическое
        //Нажатие на кнопку "Поиск":
        $(By.cssSelector(".collapse > .input-group-btn .fa")).click();
        //Проверка перехода на новую страницу: наличие фразы: "Товары, соответствующие критериям поиска":
        $(By.xpath("//*[@id='content']/h2")).shouldHave(text("Товары, соответствующие критериям поиска"));
        //Нажатие на картинку с мобильным телефоном:
        $(By.cssSelector("a > .img-responsive")).click();
        //Проверка перехода на новую страницу: наличие фразы: "Основные характеристики":
        $(By.xpath("//*[@id=\'tab-specification\']/div/table/tbody[1]/tr/th/strong")).shouldHave(text("Основные характеристики"));
        //Нажатие на стрелку вверх два раза в поле "Кол-во";
        $(By.id("input-quantity")).setValue("3");
        //Проверка значения в поле "Кол-во":
        String expFieldAmount = "3";//Ожидаемое значение
        String actFieldAmount = $(By.xpath("//*[@id=\'input-quantity\']")).getValue();//Фактическое значение
        System.out.println("Element is "+ actFieldAmount);//Фактическое выводим в логи
        Assert.assertEquals(expFieldAmount,actFieldAmount);//сравниваем ожидаемое и фактическое
        Thread.sleep(3000);//ожидание для завершения анимации на поле "сумма"
        //Проверка значения в поле "Сумма":
        String expFieldSum = "870.00 р.";//Ожидаемое значение
        String actFieldSum = $(By.cssSelector("#product > div.price-detached > span > span")).getText();//Фактическое значение
        System.out.println("Element is "+ actFieldSum);//Фактическое выводим в логи
        Assert.assertEquals(expFieldSum,actFieldSum);//сравниваем ожидаемое и фактическое
    }

//ТЕСТ C37417. Проверить наличие кнопки "Оформить заказ".
    public void C37417() throws InterruptedException {
        Thread.sleep(3000);//ожидание подгрузки сайта
        //Проверка: ожидаем на странице текст "mObilka.by"
        $("#logo > span.text-primary").shouldHave(text("mObilka.by"));
        //Ввод в поле "Поиск" значения "Nokia Lumia 800":
        $(By.name("search")).setValue("Nokia Lumia 800");
        //Проверка: ожидаем текст "Nokia Lumia 800" в поле "Поиск":
        String expFieldSearch = "Nokia Lumia 800";//Ожидаемое значение
        String actFieldSearch = $(By.name("search")).getValue();//Фактическое значение
        System.out.println("Element is " + actFieldSearch);//Фактическое выводим в логи
        Assert.assertEquals(expFieldSearch, actFieldSearch);//сравниваем ожидаемое и фактическое
        //Нажатие на кнопку "Поиск":
        $(By.cssSelector(".collapse > .input-group-btn .fa")).click();
        //Проверка перехода на новую страницу: наличие фразы: "Товары, соответствующие критериям поиска":
        $(By.xpath("//*[@id='content']/h2")).shouldHave(text("Товары, соответствующие критериям поиска"));
        //Нажатие на картинку с мобильным телефоном:
        $(By.cssSelector("a > .img-responsive")).click();
        //Проверка перехода на новую страницу: наличие фразы: "Основные характеристики":
        $(By.xpath("//*[@id=\'tab-specification\']/div/table/tbody[1]/tr/th/strong")).shouldHave(text("Основные характеристики"));
        //Нажатие на картинку с изображением телефона (мелкий ряд изображений):
        $(By.cssSelector("#content > div.row > div > div.row > div:nth-child(2) > div.thumbnails.image-additional > div > div.owl-wrapper-outer > div > div:nth-child(1) > a > img")).click();
        Thread.sleep(5000);//ожидание для подгрузки изображений
        //Проверка наличия кнопки "В корзину"
        $(By.cssSelector("#image-addon > div.btn-group.additional-buttons > button.btn.btn-primary")).shouldHave(text(" В корзину"));
        //Нажатие на кнопку "В корзину":
        $(By.cssSelector("#image-addon > div.btn-group.additional-buttons > button.btn.btn-primary")).click();
        //Проверка наличия кнопки "Оформить заказ".
        $(By.xpath("//*[@id=\'popupModal\']/div/div/div[2]/ul/li[2]/div/p[1]/a[1]")).shouldHave(text(" Оформить заказ"));
    }

//ТЕСТ C37418. Проверить наличие текста "Оформление заказа".
    public void C37418() throws InterruptedException {
        Thread.sleep(3000);//ожидание подгрузки сайта
        //Проверка: ожидаем на странице текст "mObilka.by"
        $("#logo > span.text-primary").shouldHave(text("mObilka.by"));
        //Ввод в поле "Поиск" значения "Nokia Lumia 800":
        $(By.name("search")).setValue("Nokia Lumia 800");
        //Проверка: ожидаем текст "Nokia Lumia 800" в поле "Поиск":
        String expFieldSearch = "Nokia Lumia 800";//Ожидаемое значение
        String actFieldSearch = $(By.name("search")).getValue();//Фактическое значение
        System.out.println("Element is " + actFieldSearch);//Фактическое выводим в логи
        Assert.assertEquals(expFieldSearch, actFieldSearch);//сравниваем ожидаемое и фактическое
        //Нажатие на кнопку "Поиск":
        $(By.cssSelector(".collapse > .input-group-btn .fa")).click();
        //Проверка перехода на новую страницу: наличие фразы: "Товары, соответствующие критериям поиска":
        $(By.xpath("//*[@id='content']/h2")).shouldHave(text("Товары, соответствующие критериям поиска"));
        //Нажатие на картинку с мобильным телефоном:
        $(By.cssSelector("a > .img-responsive")).click();
        //Проверка перехода на новую страницу: наличие фразы: "Основные характеристики":
        $(By.xpath("//*[@id=\'tab-specification\']/div/table/tbody[1]/tr/th/strong")).shouldHave(text("Основные характеристики"));
        //Нажатие на картинку с изображением телефона (мелкий ряд изображений):
        $(By.cssSelector("#content > div.row > div > div.row > div:nth-child(2) > div.thumbnails.image-additional > div > div.owl-wrapper-outer > div > div:nth-child(1) > a > img")).click();
        Thread.sleep(5000);//ожидание для подгрузки изображений
        //Проверка наличия кнопки "В корзину"
        $(By.cssSelector("#image-addon > div.btn-group.additional-buttons > button.btn.btn-primary")).shouldHave(text(" В корзину"));
        //Нажатие на кнопку "В корзину":
        $(By.cssSelector("#image-addon > div.btn-group.additional-buttons > button.btn.btn-primary")).click();
        //Проверка наличия кнопки "Оформить заказ".
        $(By.xpath("//*[@id=\'popupModal\']/div/div/div[2]/ul/li[2]/div/p[1]/a[1]")).shouldHave(text(" Оформить заказ"));
        Thread.sleep(3000);//ожидание прогрузки окна с оформлением заказа
        //Нажатие на кнопку "Оформить заказ":
        $(By.xpath("//*[@id=\'popupModal\']/div/div/div[2]/ul/li[2]/div/p[1]/a[1]")).click();
        //Проверка наличия текста "Оформление заказа":
        $(By.xpath("//*[@id=\"content\"]/h1")).shouldHave(text("Оформление заказа"));
    }
    */
}
