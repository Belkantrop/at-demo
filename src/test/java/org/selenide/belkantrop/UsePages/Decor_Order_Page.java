package org.selenide.belkantrop.UsePages;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.Step;
import io.qameta.allure.selenide.AllureSelenide;
import org.openqa.selenium.By;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import static com.codeborne.selenide.Selenide.$;

public class Decor_Order_Page {
    private SelenideElement TextOnDOPage = $(By.xpath("//*[@id=\"content\"]/h1"));

    @BeforeTest
    public void setUp() {
        SelenideLogger.addListener("allure", new AllureSelenide());
    }

    @AfterTest
    public void tearDown() {
        SelenideLogger.removeListener("allure");
    }

    @Step("Проверка наличия надписи: {textOnDOPage} на странице поиска")
    public void checkTextOnDOPage(String textOnDOPage){
        TextOnDOPage.shouldHave(Condition.text(textOnDOPage));
    }
}
