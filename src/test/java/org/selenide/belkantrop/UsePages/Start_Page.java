package org.selenide.belkantrop.UsePages;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.Step;
import io.qameta.allure.selenide.AllureSelenide;
import org.openqa.selenium.By;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import static com.codeborne.selenide.Selenide.$;

public class Start_Page {
    private SelenideElement logoTextOnStartPage = $(By.cssSelector("#logo > span.text-primary"));
    private SelenideElement searchFieldOnStartPage = $(By.name("search"));
    private SelenideElement searchButtonOnStartPage = $(By.cssSelector(".collapse > .input-group-btn .fa"));

    @BeforeTest
    public void setUp() {
        SelenideLogger.addListener("allure", new AllureSelenide());
    }

    @AfterTest
    public void tearDown() {
        SelenideLogger.removeListener("allure");
    }

    @Step("Проверка наличия логотипа-надписи: {logotext} на стартовой странице")
    public void checkLogoText(String logotext){
        logoTextOnStartPage.shouldHave(Condition.text(logotext));
    }

    @Step("Ввод текста {textForSearching} в поле поиска")
    public void setTextSearchField(String textForSearching){
        searchFieldOnStartPage.setValue(textForSearching);
    }

    @Step("Получение указанного значения поля поиска")
    public String getTextSearchField(){
        return searchFieldOnStartPage.getValue();
    }

    @Step("Нажатие на кнопку поиска")
    public void clickOnSearchButton(){
        searchButtonOnStartPage.click();
    }
}
