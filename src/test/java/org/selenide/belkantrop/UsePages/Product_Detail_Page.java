package org.selenide.belkantrop.UsePages;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.Step;
import io.qameta.allure.selenide.AllureSelenide;
import org.openqa.selenium.By;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import static com.codeborne.selenide.Selenide.$;

public class Product_Detail_Page {
    private SelenideElement textOnProductDetailPage = $(By.xpath("//*[@id=\'tab-specification\']/div/table/tbody[1]/tr/th/strong"));
    private SelenideElement countOfProducts = $(By.id("input-quantity"));
    private SelenideElement actFieldSumOfProducts = $(By.cssSelector("#product > div.price-detached > span > span"));
    private SelenideElement firstLittleImgOnPDPage = $(By.cssSelector("#content > div.row > div > div.row > div:nth-child(2) > div.thumbnails.image-additional > div > div.owl-wrapper-outer > div > div:nth-child(1) > a > img"));
    private SelenideElement buttonInBasketOnPDPage = $(By.cssSelector("#image-addon > div.btn-group.additional-buttons > button.btn.btn-primary"));
    private SelenideElement buttonDecorOrderOnPDPage = $(By.xpath("//*[@id=\'popupModal\']/div/div/div[2]/ul/li[2]/div/p[1]/a[1]"));

    @BeforeTest
    public void setUp() {
        SelenideLogger.addListener("allure", new AllureSelenide());
    }

    @AfterTest
    public void tearDown() {
        SelenideLogger.removeListener("allure");
    }

    @Step("Поиск текста на странице: {textOnPDPage}")
    public void checkTextOnPDPage (String textOnPDPage){
        textOnProductDetailPage.shouldHave(Condition.text(textOnPDPage));
    }

    @Step("Указываем количество = {someProducts} продукта")
    public void setSomeProducts (String someProducts){
        countOfProducts.setValue(someProducts);
    }

    @Step("Получение указанного количества продукта")
    public String getCountOfSomeProducts (){
        return countOfProducts.getValue();
    }

    @Step("Получение стоимости за указанное количество продукта")
    public String getAmountOfSomeProducts (){
        return actFieldSumOfProducts.getText();
    }

    @Step("Нажатие на 1 элемент типа IMG в классе owl-wrapper-outer")
    public void clickOnFirstLittleImg (){
        firstLittleImgOnPDPage.click();
    }

    @Step("Проверка наличия надписи: {textButtonInBasket} на кнопке В корзину")
    public void checkTextOnButtonInBasket(String textButtonInBasket){
        buttonInBasketOnPDPage.shouldHave(Condition.text(textButtonInBasket));
    }

    @Step("Нажатие на кнопку В корзину")
    public void clickButtonInBasket (){
        buttonInBasketOnPDPage.click();
    }

    @Step("Проверка наличия надписи: {textButtonDecorOrder} на кнопке Оформить заказ")
    public void checkTextOnButtonDecorOrder(String textButtonDecorOrder){
        buttonDecorOrderOnPDPage.shouldHave(Condition.text(textButtonDecorOrder));
    }

    @Step("Нажатие на кнопку Оформить заказ")
    public void clickOnButtonDecorOrder(){
        buttonDecorOrderOnPDPage.click();
    }
}
