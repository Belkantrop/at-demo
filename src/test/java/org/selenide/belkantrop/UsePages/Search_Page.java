package org.selenide.belkantrop.UsePages;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.Step;
import io.qameta.allure.selenide.AllureSelenide;
import org.openqa.selenium.By;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import static com.codeborne.selenide.Selenide.$;

public class Search_Page {
    private SelenideElement textOnSearchPage = $(By.xpath("//*[@id='content']/h2"));
    private SelenideElement firstFindElementInRowProduct = $(By.cssSelector("a > .img-responsive"));

    @BeforeTest
    public void setUp() {
        SelenideLogger.addListener("allure", new AllureSelenide());
    }

    @AfterTest
    public void tearDown() {
        SelenideLogger.removeListener("allure");
    }

    @Step("Проверка наличия надписи: {validProducts} на странице поиска")
    public void checkTextOnSearchPage(String validProducts) {
        textOnSearchPage.shouldHave(Condition.text(validProducts));
        }

    @Step("Нажатие на 1 найденный элемент с типом img в классе RowProduct")
    public void clickOnFirstElementRowProduct(){
        firstFindElementInRowProduct.click();
        }
}
